from django.urls import path
from rest_framework.routers import SimpleRouter  # new

from .views import MovieViewSet  # new

app_name = 'movies'

router = SimpleRouter() # new
router.register('movies', MovieViewSet, basename='movies') # new

urlpatterns = router.urls # new
